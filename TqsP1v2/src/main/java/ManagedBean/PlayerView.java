/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManagedBean;

import boundary.PlayerFacade;
import boundary.TeamFacade;
import domain.Player;
import domain.Team;
import domain.PlayerhasTeam;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.faces.context.FacesContext;

/**
 *
 * @author hugofernandes
 */
@Named(value = "playerView")
@Dependent
public class PlayerView {

    @EJB
    private PlayerFacade playerFacade;
    @EJB
    private TeamFacade teamFacade;
    private static Player player = new Player();
    private static String currentTeam = "";

    /**
     * Creates a new instance of PlayerView
     */
    public PlayerView() {
        teamFacade = new TeamFacade();
        playerFacade = new PlayerFacade();
    }

    public void searchPlayer(String username) throws IOException {
        currentTeam = "";
        List<Player> players = playerFacade.findAll();
        String playerUsername = "";
        boolean found = false;

        for (int i = 0; i < players.size(); i++) {
            playerUsername = players.get(i).getUsername();

            if (playerUsername.equals(username)) {
                player = players.get(i);
                found = true;
            }
        }

        if (found) {
            Collection<PlayerhasTeam> allPlayerhasTeam = player.getPlayerhasTeamCollection();

            Iterator iterator = allPlayerhasTeam.iterator();
            while (iterator.hasNext()) {
                PlayerhasTeam playerhasteam = (PlayerhasTeam) iterator.next();
                if (playerhasteam.getPlayer().getIdPlayer() == player.getIdPlayer()) {
                    List<Team> allTeams = teamFacade.findAll();

                    for (int i = 0; i < allTeams.size(); i++) {

                        if (allTeams.get(i).getIdTeam() == playerhasteam.getTeam().getIdTeam()) {
                            if (i != 0) {
                                currentTeam = currentTeam + " , ";
                            }
                            currentTeam = currentTeam + allTeams.get(i).getTeamName() + "";
                        }
                    }

                }
            }

            if (currentTeam == "") {
                currentTeam = "No team";
            }

            FacesContext.getCurrentInstance().getExternalContext().redirect("playerInfo.xhtml");
        }

    }

    public String getCurrentTeam() {
        return currentTeam;
    }

    public Player getPlayer() {
        System.out.println("AQUI..." + player.getUsername());
        return player;
    }
}
