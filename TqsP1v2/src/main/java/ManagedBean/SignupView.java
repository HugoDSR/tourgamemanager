/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManagedBean;

import boundary.PlayerFacade;
import domain.Player;
import java.io.IOException;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author hugofernandes
 */
@Named(value = "signupView")
@Dependent
public class SignupView {

    @EJB
    private PlayerFacade playerFacade;
    public static String username = "";
    private static String warning_login = "";
    private static String warning_signup = "";

    /**
     * Creates a new instance of SignupView
     */
    public SignupView() {
        playerFacade = new PlayerFacade();
    }

    public String getUsername() {
        return username;
    }

    public String getWarning_login() {
        return warning_login;
    }

    public String getWarning_signup() {
        return warning_signup;
    }

    public void signup(String username, String password, String email) throws IOException {
        boolean valido = true;

        if (!username.equals("") && !password.equals("") && !email.equals("")) {
            List<Player> players = playerFacade.findAll();

            for (Player player : players) {
                if (player.getUsername().equals(username)) {
                    valido = false;
                }
                warning_signup = "The username you're trying to sign up already exists. Please, try another one!";
            }

        } else {
            valido = false;
            warning_signup = "The fields shouldn't be empty!";
        }

        if (valido) {
            warning_signup = "";
            Player p = new Player();
            p.setUsername(username);
            p.setPassword(password);
            p.setEmail(email);

            playerFacade.create(p);
            this.username = username;
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml#signup");
        }

    }

    public void logout() throws IOException {
        this.username = "";
        TeamView.admin = "";
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        ec.redirect(((HttpServletRequest) ec.getRequest()).getRequestURI());
    }

    public void login(String username, String password) throws IOException {
        boolean valido = false;

        for (Player p : playerFacade.findAll()) {
            if (p.getUsername().equals(username) && p.getPassword().equals(password)) {
                valido = true;
                this.username = username;
            }
        }

        if (valido) {
            warning_login = "";
            ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            ec.redirect(((HttpServletRequest) ec.getRequest()).getRequestURI());
        } else {
            warning_login = "Your data are wrong!";
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml#login");
        }
    }
}
