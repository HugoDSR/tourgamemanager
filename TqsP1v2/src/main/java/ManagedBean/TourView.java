package ManagedBean;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import boundary.GameFacade;
import boundary.MatchesFacade;
import boundary.PlayerFacade;
import boundary.PlayerhasTeamFacade;
import boundary.TeamFacade;
import boundary.TourFacade;
import boundary.TourTypeFacade;
import boundary.TourhasTeamFacade;
import domain.Game;
import domain.Matches;
import domain.Tour;
import domain.TourhasTeam;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Random;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import domain.Player;
import domain.Team;
import domain.PlayerhasTeam;
import domain.TourType;
import java.util.Collections;
import java.util.Iterator;
import javax.ejb.EJBException;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.eclipse.persistence.jpa.jpql.Assert;

/**
 *
 * @author hugofernandes
 */
@Named(value = "tourView")
@Dependent
public class TourView {

         @EJB
         private TourFacade tourFacade;
         @EJB
         private PlayerFacade playerFacade;
         @EJB
         private TeamFacade teamFacade;
         @EJB
         private TourhasTeamFacade tourhasTeamFacade;
         @EJB
         private MatchesFacade matchFacade;
         @EJB
         private GameFacade gameFacade;
         @EJB
         private TourTypeFacade tourTypeFacade;
         @EJB
         private PlayerhasTeamFacade playerhasTeamFacade;

         private static Tour tour;
         private static int slotsAvailable;
         private List<Tour> lTournament;
         private List<Tour> mylTournament;
         private List<Matches> lMatches;
         private List<String> teamsOnTour;
         private static String response;
         private int selectedTour;
         private String teamName;
         private boolean simulation;
         private static String tourState = "Starting";
         private static String tourWinner = "";
         private static String warning_createTour = "";         
         private static String warning_joinTour = "";

         /**
          * Creates a new instance of TourManagedBean
          */
         public TourView() {
                  lTournament = new ArrayList();
                  lMatches = new ArrayList();
                  simulation = false;
                  teamsOnTour = new ArrayList();
         }

         public String getWarning_createTour() {
                  return warning_createTour;
         }
         
         public String getWarning_joinTour(){
                  return warning_joinTour;
         }

         public void getInfo(Tour tour) throws IOException {
                  this.tour = tour;
                  // VIR AQUI MUDAR ISTO
                  slotsAvailable = this.tour.getGameidGame().getMaxPlayer();
                  FacesContext.getCurrentInstance().getExternalContext().redirect("specificTournament.xhtml#tournament");
         }

         public int getSlotsAvailable() {
                  return slotsAvailable;
         }

         public Tour getTour() {
                  return tour;
         }

         public List<Tour> getlTournament() {
                  return tourFacade.findAll();
         }

         public void setlMatches(List<Matches> lMatches) {
                  this.lMatches = lMatches;
         }

         public static String getResponse() {
                  return response;
         }

         /**
          * TODO: test functions; TODO: trabalhar com ids; TODO: modelar codigo,
          * tirar repetido.
          */
         public void goToCreateTour() throws IOException {
                  FacesContext.getCurrentInstance().getExternalContext().redirect("createTour.xhtml#tour");
         }

         public void createTour(String sessionPlayer, String tourName, String desc,
                         String dateTime, String buyin, String idGame, String idTourStyle) throws IOException {

                  boolean[] bol = new boolean[2];

                  for (Game game : gameFacade.findAll()) {
                           if (game.getIdGame() == Integer.parseInt(idGame)) {
                                    warning_createTour = "Erro: Numero de jogo inexistente";
                                    FacesContext.getCurrentInstance().getExternalContext().redirect("createTour.xhtml");
                           }
                  }

                  for (TourType tt : tourTypeFacade.findAll()) {
                           if (tt.getIdTourType() == Integer.parseInt(idTourStyle)) {
                                    warning_createTour = "Erro: Tipo de jogo inexistente";
                                    FacesContext.getCurrentInstance().getExternalContext().redirect("createTour.xhtml");
                           }
                  }

                  boolean success = false;
                  Tour tempTour = new Tour();
                  try {
                           // create tour with interface info
                           tempTour.setName(tourName);
                           tempTour.setDescription(desc);
                           tempTour.setDateTime(new Date());   //  dateTime
                           tempTour.setBuyin(Float.parseFloat(buyin));
                           tempTour.setGameidGame(gameFacade.find(Integer.parseInt(idGame)));
                           tempTour.setTourTypeidTourType(tourTypeFacade.find(Integer.parseInt(idTourStyle)));
                           tempTour.setPrizepool(Float.parseFloat(buyin) * 12);
                           tourFacade.create(tempTour);
                           success = true;
                  } catch (EJBException e) {
                           @SuppressWarnings("ThrowableResultIgnored")
                           Exception cause = e.getCausedByException();
                           if (cause instanceof ConstraintViolationException) {
                                    @SuppressWarnings("ThrowableResultIgnored")
                                    ConstraintViolationException cve = (ConstraintViolationException) e.getCausedByException();
                                    for (Iterator<ConstraintViolation<?>> it = cve.getConstraintViolations().iterator(); it.hasNext();) {
                                             ConstraintViolation<? extends Object> v = it.next();
                                             System.err.println(v);
                                             System.err.println("==>>" + v.getMessage());
                                    }
                           }
                           Assert.fail("ejb exception");
                  }

                  if (success) {
                           FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
                  }
         }

         private Tour findTourByName(String tourName) {
                  Tour tempTour = null;  // ref reset inside this function

                  List<Tour> tours = tourFacade.findAll();
                  // run all tour
                  for (int i = 0; i < tours.size(); i++) {
                           //search tour
                           if (tours.get(i).getName().equals(tourName)) {
                                    tempTour = tours.get(i);
                           }
                  }
                  return tempTour;
         }

         private Player findPlayerByName(String playerName) {
                  Player tmpPlayer = null; // ref reset inside this function

                  // get lists
                  List<Player> players = playerFacade.findAll();

                  // run all players
                  for (int i = 0; i < players.size(); i++) {
                           //search player
                           if (players.get(i).getUsername().equals(playerName)) {
                                    tmpPlayer = players.get(i);
                           }
                  }
                  return tmpPlayer;
         }

         private Team findTeamByName(String teamName) {
                  Team tempteam = null; // ref reset inside this function

                  // get lists
                  List<Team> teams = teamFacade.findAll();

                  // run all teams
                  for (int i = 0; i < teams.size(); i++) {
                           //search team
                           if (teams.get(i).getTeamName().equals(teamName)) {
                                    tempteam = teams.get(i);
                           }
                  }
                  return tempteam;
         }

         private Team findTeamById(int teamId) {
                  Team tempteam = null; // ref reset inside this function

                  // get lists
                  List<Team> teams = teamFacade.findAll();

                  // run all teams
                  for (int i = 0; i < teams.size(); i++) {
                           //search team
                           if (teams.get(i).getIdTeam().equals(teamId)) {
                                    tempteam = teams.get(i);
                           }
                  }
                  return tempteam;
         }

         private boolean setPlayerHasTour(String sessionPlayer, Tour tour) {  //criar obj na tabela Player_has_Tour

                  // to get ID from player
                  Player playerFound = findPlayerByName(sessionPlayer);

                  if (playerFound != null) {   //player encontrado

                           playerFacade.find(playerFound.getIdPlayer()).getTourCollection().add(tour);
                           tourFacade.find(tour.getIdTour()).getPlayerCollection().add(playerFound);

// rever com Hugo, onde está a facade de Player_has_Tour? ... ou como adiciono a entrada?
                           /*playHasTeamFacade.create( 
                    new PalyerHasTour( playerFound.getIdPlayer(), tour )  
            );*/
                           return true;
                  }

                  return false;
         }

         public void signTeamToTour(String tourName, String teamName) {

                  // find tour
                  Tour tmpTour = findTourByName(tourName);
                  // find team
                  Team tmpTeam = findTeamByName(teamName);

                  if (tmpTour != null) {

                           if (tmpTeam != null) {

                                    // create obj tour_has_team, set confirmation to false
                                    TourhasTeam tourTeam = new TourhasTeam();
                                    tourTeam.setTour(tmpTour);
                                    tourTeam.setTeam(tmpTeam);
                                    tourTeam.setConfirmation(Boolean.FALSE); // confirmacao quando se aproxima do tourneio
                                    tourhasTeamFacade.create(tourTeam);

                                    response = tmpTeam.getTeamName() + " has sign to tournament " + tmpTour.getName();

                           } else {
                                    response = "Team not found";
                           }

                  } else {
                           response = "Tour not found";
                  }
         }

         private List<TourhasTeam> findTeamsByTour(int idTour) {

                  List<TourhasTeam> teamsInTour = tourhasTeamFacade.findAll();
                  List<TourhasTeam> filterTeamList = null;

                  // run all teams
                  for (int i = 0; i < teamsInTour.size(); i++) {
                           //search team in tour "tourName" && confirmation = true;
                           if (teamsInTour.get(i).getTour().getIdTour() == idTour) {
                                    filterTeamList.add(teamsInTour.get(i));
                           }
                  }

                  return filterTeamList;
         }

         public void goToMyTour() throws IOException {
                  FacesContext.getCurrentInstance().getExternalContext().redirect("tourManager.xhtml#tour");
         }

         public void startTour(int idTour) throws IOException {
                  selectedTour = idTour;
                  FacesContext.getCurrentInstance().getExternalContext().redirect("tourDetails.xhtml#tour");
         }

         public Collection<Tour> getMylTournament(String user) {
                  return findPlayerByName(user).getTourCollection();
         }

         public String getTourState() {
                  return tourState;
         }

         public List<Matches> getlMatches() {
                  return matchFacade.findAll();
         }

         public String getTeamOnMatchName(int teamId) {

                  teamName = findTeamById(teamId).getTeamName();

                  return teamName;
         }

         public String getTourWinner() {
                  return tourWinner;
         }

         public void simulation() {
                  tourState = "Ended";
                  simulation = true;

                  List<Matches> list = matchFacade.findAll();
                  if (teamsOnTour.isEmpty()) {
                           teamsOnTour.add(findTeamById(list.get(0).getTeamA()).getTeamName());
                  }
                  for (Matches match : list) {
                           if (!teamsOnTour.contains(findTeamById(match.getTeamA()).getTeamName())) {
                                    teamsOnTour.add(findTeamById(match.getTeamA()).getTeamName());
                           }

                           if (!teamsOnTour.contains(findTeamById(match.getTeamB()).getTeamName())) {
                                    teamsOnTour.add(findTeamById(match.getTeamB()).getTeamName());
                           }
                  }
                  Random rand = new Random();

                  int randomNum = rand.nextInt(teamsOnTour.size());

                  tourWinner = "Winner: " + teamsOnTour.get(randomNum);
         }

         private String getTeam(String username) {
                  int playerId = 0;
                  List<Player> playerList = playerFacade.findAll();
                  List<PlayerhasTeam> playerHasTeamList = playerhasTeamFacade.findAll();
                  for (Player player : playerList) {
                           if (player.getUsername().equalsIgnoreCase(username)) {
                                    playerId = player.getIdPlayer();
                           }
                  }
                  for (PlayerhasTeam player : playerHasTeamList) {
                           if (player.getPlayer().getIdPlayer() == playerId) {
                                    byte[] buffer = player.getLider();
                                    String isLeader = "";
                                    for (byte b : buffer) {
                                             isLeader += (char) b;
                                    }

                                    if (isLeader.equals("1")) {
                                             return player.getTeam().getTeamName();
                                    }
                           }
                  }
                  return "";
         }

         public int getTour(String tourName) {
                  return findTourByName(tourName).getIdTour();
         }

         public void registTeam(String username, String tourName) throws IOException {
                  boolean success = false;
                  String team = getTeam(username);

                  for(String tourTeam : getTourTeams(tourName)){
                           if(tourTeam.equalsIgnoreCase(team)){
                                    warning_joinTour = "Erro: Equipa já inscrita em torneio";
                                    FacesContext.getCurrentInstance().getExternalContext().redirect("specificTournament.xhtml");
                           }
                  }
                  
                  TourhasTeam tourTeam = new TourhasTeam(findTourByName(tourName).getIdTour(), findTeamByName(team).getIdTeam());
                  try {
                           tourTeam.setConfirmation(true);
                           tourTeam.setTeam(findTeamByName(team));
                           tourTeam.setTour(findTourByName(tourName));
                           tourhasTeamFacade.create(tourTeam);
                           success = true;
                  } catch (EJBException e) {
                           @SuppressWarnings("ThrowableResultIgnored")
                           Exception cause = e.getCausedByException();
                           if (cause instanceof ConstraintViolationException) {
                                    @SuppressWarnings("ThrowableResultIgnored")
                                    ConstraintViolationException cve = (ConstraintViolationException) e.getCausedByException();
                                    for (Iterator<ConstraintViolation<?>> it = cve.getConstraintViolations().iterator(); it.hasNext();) {
                                             ConstraintViolation<? extends Object> v = it.next();
                                             System.err.println(v);
                                             System.err.println("==>>" + v.getMessage());
                                    }
                           }
                           Assert.fail("ejb exception");
                  }

                  if (success) {
                           FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml#tour");
                  }
         }

         public List<String> getTourTeams(String tourName) {
                  List<String> tourTeams = new ArrayList();
                  Tour tour = findTourByName(tourName);

                  for (TourhasTeam team : tourhasTeamFacade.findAll()) {
                           if (team.getTour().getIdTour() == tour.getIdTour() && team.getTour().getDescription().equalsIgnoreCase(tour.getDescription())) {
                                    tourTeams.add(team.getTeam().getTeamName());
                           }
                  }
                  return tourTeams;
         }

}
