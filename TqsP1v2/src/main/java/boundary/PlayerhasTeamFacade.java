/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boundary;

import domain.PlayerhasTeam;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author hugofernandes
 */
@Stateless
public class PlayerhasTeamFacade extends AbstractFacade<PlayerhasTeam> {
    @PersistenceContext(unitName = "com.mycompany_TqsP1_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PlayerhasTeamFacade() {
        super(PlayerhasTeam.class);
    }
    
}
