/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boundary;

import domain.PlayerhasTeam;
import domain.PlayerhasTeamPK;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.PathSegment;

/**
 *
 * @author hugo
 */
@Stateless
@Path("playerhasteam")
public class PlayerhasTeamFacadeREST extends AbstractFacade<PlayerhasTeam> {

         @PersistenceContext(unitName = "com.mycompany_TqsP1_war_1.0-SNAPSHOTPU")
         private EntityManager em;

         private PlayerhasTeamPK getPrimaryKey(PathSegment pathSegment) {
                  /*
                   * pathSemgent represents a URI path segment and any associated matrix parameters.
                   * URI path part is supposed to be in form of 'somePath;playeridPlayer=playeridPlayerValue;teamidTeam=teamidTeamValue'.
                   * Here 'somePath' is a result of getPath() method invocation and
                   * it is ignored in the following code.
                   * Matrix parameters are used as field names to build a primary key instance.
                   */
                  domain.PlayerhasTeamPK key = new domain.PlayerhasTeamPK();
                  javax.ws.rs.core.MultivaluedMap<String, String> map = pathSegment.getMatrixParameters();
                  java.util.List<String> playeridPlayer = map.get("playeridPlayer");
                  if (playeridPlayer != null && !playeridPlayer.isEmpty()) {
                           key.setPlayeridPlayer(new java.lang.Integer(playeridPlayer.get(0)));
                  }
                  java.util.List<String> teamidTeam = map.get("teamidTeam");
                  if (teamidTeam != null && !teamidTeam.isEmpty()) {
                           key.setTeamidTeam(new java.lang.Integer(teamidTeam.get(0)));
                  }
                  return key;
         }

         public PlayerhasTeamFacadeREST() {
                  super(PlayerhasTeam.class);
         }

         @POST
         @Override
         @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
         public void create(PlayerhasTeam entity) {
                  super.create(entity);
         }

         @PUT
         @Path("{id}")
         @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
         public void edit(@PathParam("id") PathSegment id, PlayerhasTeam entity) {
                  super.edit(entity);
         }

         @DELETE
         @Path("{id}")
         public void remove(@PathParam("id") PathSegment id) {
                  domain.PlayerhasTeamPK key = getPrimaryKey(id);
                  super.remove(super.find(key));
         }

         @GET
         @Path("{id}")
         @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
         public PlayerhasTeam find(@PathParam("id") PathSegment id) {
                  domain.PlayerhasTeamPK key = getPrimaryKey(id);
                  return super.find(key);
         }

         @GET
         @Override
         @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
         public List<PlayerhasTeam> findAll() {
                  return super.findAll();
         }

         @GET
         @Path("{from}/{to}")
         @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
         public List<PlayerhasTeam> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
                  return super.findRange(new int[]{from, to});
         }

         @GET
         @Path("count")
         @Produces(MediaType.TEXT_PLAIN)
         public String countREST() {
                  return String.valueOf(super.count());
         }

         @Override
         protected EntityManager getEntityManager() {
                  return em;
         }
         
}
