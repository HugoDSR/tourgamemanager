/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author hugofernandes
 */
@Entity
@Table(name = "Matches")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Matches.findAll", query = "SELECT m FROM Matches m"),
    @NamedQuery(name = "Matches.findByIdMatch", query = "SELECT m FROM Matches m WHERE m.idMatch = :idMatch"),
    @NamedQuery(name = "Matches.findByTeamA", query = "SELECT m FROM Matches m WHERE m.teamA = :teamA"),
    @NamedQuery(name = "Matches.findByTeamB", query = "SELECT m FROM Matches m WHERE m.teamB = :teamB"),
    @NamedQuery(name = "Matches.findByWinner", query = "SELECT m FROM Matches m WHERE m.winner = :winner"),
    @NamedQuery(name = "Matches.findByDateTime", query = "SELECT m FROM Matches m WHERE m.dateTime = :dateTime")})
public class Matches implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idMatch")
    private Integer idMatch;
    @Basic(optional = false)
    @NotNull
    @Column(name = "teamA")
    private int teamA;
    @Basic(optional = false)
    @NotNull
    @Column(name = "teamB")
    private int teamB;
    @Column(name = "winner")
    private Integer winner;
    @Basic(optional = false)
    @NotNull
    @Column(name = "dateTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateTime;
    @JoinTable(name = "Tour_has_Match", joinColumns = {
        @JoinColumn(name = "Match_idMatch", referencedColumnName = "idMatch")}, inverseJoinColumns = {
        @JoinColumn(name = "Tour_idTour", referencedColumnName = "idTour")})
    @ManyToMany
    private Collection<Tour> tourCollection;

    public Matches() {
    }

    public Matches(Integer idMatch) {
        this.idMatch = idMatch;
    }

    public Matches(Integer idMatch, int teamA, int teamB, Date dateTime) {
        this.idMatch = idMatch;
        this.teamA = teamA;
        this.teamB = teamB;
        this.dateTime = dateTime;
    }

    public Integer getIdMatch() {
        return idMatch;
    }

    public void setIdMatch(Integer idMatch) {
        this.idMatch = idMatch;
    }

    public int getTeamA() {
        return teamA;
    }

    public void setTeamA(int teamA) {
        this.teamA = teamA;
    }

    public int getTeamB() {
        return teamB;
    }

    public void setTeamB(int teamB) {
        this.teamB = teamB;
    }

    public Integer getWinner() {
        return winner;
    }

    public void setWinner(Integer winner) {
        this.winner = winner;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    @XmlTransient
    public Collection<Tour> getTourCollection() {
        return tourCollection;
    }

    public void setTourCollection(Collection<Tour> tourCollection) {
        this.tourCollection = tourCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMatch != null ? idMatch.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Matches)) {
            return false;
        }
        Matches other = (Matches) object;
        if ((this.idMatch == null && other.idMatch != null) || (this.idMatch != null && !this.idMatch.equals(other.idMatch))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "domain.Matches[ idMatch=" + idMatch + " ]";
    }
    
}
