/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author hugofernandes
 */
@Embeddable
public class PlayerhasTeamPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "Player_idPlayer")
    private int playeridPlayer;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Team_idTeam")
    private int teamidTeam;

    public PlayerhasTeamPK() {
    }

    public PlayerhasTeamPK(int playeridPlayer, int teamidTeam) {
        this.playeridPlayer = playeridPlayer;
        this.teamidTeam = teamidTeam;
    }

    public int getPlayeridPlayer() {
        return playeridPlayer;
    }

    public void setPlayeridPlayer(int playeridPlayer) {
        this.playeridPlayer = playeridPlayer;
    }

    public int getTeamidTeam() {
        return teamidTeam;
    }

    public void setTeamidTeam(int teamidTeam) {
        this.teamidTeam = teamidTeam;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) playeridPlayer;
        hash += (int) teamidTeam;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PlayerhasTeamPK)) {
            return false;
        }
        PlayerhasTeamPK other = (PlayerhasTeamPK) object;
        if (this.playeridPlayer != other.playeridPlayer) {
            return false;
        }
        if (this.teamidTeam != other.teamidTeam) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "domain.PlayerhasTeamPK[ playeridPlayer=" + playeridPlayer + ", teamidTeam=" + teamidTeam + " ]";
    }
    
}
