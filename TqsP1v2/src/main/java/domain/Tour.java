/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author hugofernandes
 */
@Entity
@Table(name = "Tour")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tour.findAll", query = "SELECT t FROM Tour t"),
    @NamedQuery(name = "Tour.findByIdTour", query = "SELECT t FROM Tour t WHERE t.idTour = :idTour"),
    @NamedQuery(name = "Tour.findByName", query = "SELECT t FROM Tour t WHERE t.name = :name"),
    @NamedQuery(name = "Tour.findByDescription", query = "SELECT t FROM Tour t WHERE t.description = :description"),
    @NamedQuery(name = "Tour.findByDateTime", query = "SELECT t FROM Tour t WHERE t.dateTime = :dateTime"),
    @NamedQuery(name = "Tour.findByBuyin", query = "SELECT t FROM Tour t WHERE t.buyin = :buyin"),
    @NamedQuery(name = "Tour.findByPrizepool", query = "SELECT t FROM Tour t WHERE t.prizepool = :prizepool")})
public class Tour implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idTour")
    private Integer idTour;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "name")
    private String name;
    @Size(max = 250)
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
    @NotNull
    @Column(name = "dateTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateTime;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "buyin")
    private Float buyin;
    @Column(name = "prizepool")
    private Float prizepool;
    @ManyToMany(mappedBy = "tourCollection")
    private Collection<Player> playerCollection;
    @ManyToMany(mappedBy = "tourCollection")
    private Collection<Team> teamCollection;
    @ManyToMany(mappedBy = "tourCollection")
    private Collection<Matches> matchesCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tour")
    private Collection<TourhasTeam> tourhasTeamCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "touridTour")
    private Collection<TourHistory> tourHistoryCollection;
    @JoinColumn(name = "TourType_idTourType", referencedColumnName = "idTourType")
    @ManyToOne(optional = false)
    private TourType tourTypeidTourType;
    @JoinColumn(name = "Game_idGame", referencedColumnName = "idGame")
    @ManyToOne(optional = false)
    private Game gameidGame;

    public Tour() {
    }

    public Tour(Integer idTour) {
        this.idTour = idTour;
    }

    public Tour(Integer idTour, String name, Date dateTime) {
        this.idTour = idTour;
        this.name = name;
        this.dateTime = dateTime;
    }

    public Collection<Team> getTeamCollection() {
        return teamCollection;
    }

    public void setTeamCollection(Collection<Team> teamCollection) {
        this.teamCollection = teamCollection;
    }
    
    public Integer getIdTour() {
        return idTour;
    }

    public void setIdTour(Integer idTour) {
        this.idTour = idTour;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public Float getBuyin() {
        return buyin;
    }

    public void setBuyin(Float buyin) {
        this.buyin = buyin;
    }

    public Float getPrizepool() {
        return prizepool;
    }

    public void setPrizepool(Float prizepool) {
        this.prizepool = prizepool;
    }

    @XmlTransient
    public Collection<Player> getPlayerCollection() {
        return playerCollection;
    }

    public void setPlayerCollection(Collection<Player> playerCollection) {
        this.playerCollection = playerCollection;
    }

    @XmlTransient
    public Collection<Matches> getMatchesCollection() {
        return matchesCollection;
    }

    public void setMatchesCollection(Collection<Matches> matchesCollection) {
        this.matchesCollection = matchesCollection;
    }

    @XmlTransient
    public Collection<TourhasTeam> getTourhasTeamCollection() {
        return tourhasTeamCollection;
    }

    public void setTourhasTeamCollection(Collection<TourhasTeam> tourhasTeamCollection) {
        this.tourhasTeamCollection = tourhasTeamCollection;
    }

    @XmlTransient
    public Collection<TourHistory> getTourHistoryCollection() {
        return tourHistoryCollection;
    }

    public void setTourHistoryCollection(Collection<TourHistory> tourHistoryCollection) {
        this.tourHistoryCollection = tourHistoryCollection;
    }

    public TourType getTourTypeidTourType() {
        return tourTypeidTourType;
    }

    public void setTourTypeidTourType(TourType tourTypeidTourType) {
        this.tourTypeidTourType = tourTypeidTourType;
    }

    public Game getGameidGame() {
        return gameidGame;
    }

    public void setGameidGame(Game gameidGame) {
        this.gameidGame = gameidGame;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTour != null ? idTour.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tour)) {
            return false;
        }
        Tour other = (Tour) object;
        if ((this.idTour == null && other.idTour != null) || (this.idTour != null && !this.idTour.equals(other.idTour))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "domain.Tour[ idTour=" + idTour + " ]";
    }
    
}
