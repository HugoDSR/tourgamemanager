/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author hugofernandes
 */
@Embeddable
public class TourhasTeamPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "Tour_idTour")
    private int touridTour;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Team_idTeam")
    private int teamidTeam;

    public TourhasTeamPK() {
    }

    public TourhasTeamPK(int touridTour, int teamidTeam) {
        this.touridTour = touridTour;
        this.teamidTeam = teamidTeam;
    }

    public int getTouridTour() {
        return touridTour;
    }

    public void setTouridTour(int touridTour) {
        this.touridTour = touridTour;
    }

    public int getTeamidTeam() {
        return teamidTeam;
    }

    public void setTeamidTeam(int teamidTeam) {
        this.teamidTeam = teamidTeam;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) touridTour;
        hash += (int) teamidTeam;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TourhasTeamPK)) {
            return false;
        }
        TourhasTeamPK other = (TourhasTeamPK) object;
        if (this.touridTour != other.touridTour) {
            return false;
        }
        if (this.teamidTeam != other.teamidTeam) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "domain.TourhasTeamPK[ touridTour=" + touridTour + ", teamidTeam=" + teamidTeam + " ]";
    }
    
}
