/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JUnitTest;

import ManagedBean.SignupView;
import ManagedBean.TeamView;
import domain.Player;
import domain.Team;
import domain.PlayerhasTeam;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author acardoso
 */
public class TeamViewTest {
    
    private Player p1, p2;
    private List<Player> lPlayers;
    private PlayerhasTeam pt1, pt2;
    private List<PlayerhasTeam> lPlayHasTeam;
    
    public TeamViewTest() {
    }
    
    @Before
    public void setUp() {
        
        p1 = new Player(0, "user1", "userpass", "user@asd.com");
        p2 = new Player(1, "user2", "userpass", "user@asd.com");
        lPlayers = new ArrayList<>();
        lPlayers.add( p1 );
        lPlayers.add( p2 );
        pt1 = new PlayerhasTeam(0, 0);
        pt2 = new PlayerhasTeam(1, 1);
        lPlayHasTeam = new ArrayList<>();
        lPlayHasTeam.add( pt1 );
        lPlayHasTeam.add( pt2 );
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getWarning method, of class TeamView.
     */
    @Test
    public void addPlayerToTeam() {
        System.out.println("getWarning");
        
        String admin ="";
        int idTeam = 2; //team.getIdTeam();
        List<Player> players = lPlayers; //playerFacade.findAll();
        List<PlayerhasTeam> playerHasTeam = lPlayHasTeam;//playerhasTeamFacade.findAll();
        boolean entered = false;

        for (int j = 0; j < playerHasTeam.size(); j++) {
            System.out.println(playerHasTeam.get(j).getPlayer().getUsername() + " : " + SignupView.username + " : " + playerHasTeam.get(j).getPlayer().getUsername().equals(SignupView.username));

            if (playerHasTeam.get(j).getPlayer().getUsername().equals(SignupView.username) && playerHasTeam.get(j).getTeam().getIdTeam().equals(idTeam)) {
                entered = true;
                byte[] buffer = playerHasTeam.get(j).getLider();
                String isLeader = "";
                for (byte b : buffer) {
                    isLeader += (char) b;
                }

                if (isLeader.equals("1")) {
                    admin = playerHasTeam.get(j).getNickname();
                } else {
                    admin = "";
                }

                break;
            }
        }

        if (!entered) {
            admin = "";
        }
    }

    
}
