/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JUnitTest;

import domain.Player;
import domain.Team;
import domain.Matches;
import domain.Tour;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author acardoso
 */
public class TourViewTest {

    private Tour t1, t2;
    private Player p1, p2;
    private Team te1, te2;
    private List<Tour> lTours;
    private List<Player> lPlayers;
    private List<Team> lTeams;

    public TourViewTest() {
    }

    @Before
    public void setUp() {

        t1 = new Tour(0, "Tour1", new Date(System.currentTimeMillis()));
        t2 = new Tour(1, "Tour2", new Date(System.currentTimeMillis()));
        lTours = new ArrayList<>();
        lTours.add(t1);
        lTours.add(t2);

        p1 = new Player(0, "user1", "userpass", "user@asd.com");
        p2 = new Player(1, "user2", "userpass", "user@asd.com");
        lPlayers = new ArrayList<>();
        lPlayers.add(p1);
        lPlayers.add(p2);

        te1 = new Team(0, "Team1");
        te2 = new Team(1, "Team2");
        lTeams = new ArrayList<>();
        lTeams.add(te1);
        lTeams.add(te2);

        System.out.println("@Before - setUp");
    }

    @After
    public void tearDown() {
        lTours.clear();
        System.out.println("@After - tearDown");
    }

    @Test
    public void findTourByName() {
        System.out.println("Testing... findTourByName( Tour2 )");

        Tour tempTour = null;  // ref reset inside this function

        List<Tour> tours = lTours; //tourFacade.findAll();
        // run all tour
        for (int i = 0; i < tours.size(); i++) {
            //search tour
            if (tours.get(i).getName().equals(t2.getName())) {
                tempTour = tours.get(i);
            }
        }
        assertEquals(tempTour, t2);
    }

    @Test
    public void findPlayerByName() {
        System.out.println("Testing... findPlayerByName( user2 )");

        Player tmpPlayer = null; // ref reset inside this function

        // get lists
        List<Player> players = lPlayers; //playerFacade.findAll();

        // run all players
        for (int i = 0; i < players.size(); i++) {
            //search player
            if (players.get(i).getUsername().equals(p2.getUsername())) {
                tmpPlayer = players.get(i);
            }
        }
        assertEquals(tmpPlayer, p2);
    }

    @Test
    public void findTeamByName() {
        System.out.println("Testing... findTeamByName( Team2 )");

        Team tempteam = null; // ref reset inside this function

        // get lists
        List<Team> teams = lTeams;//teamFacade.findAll();

        // run all teams
        for (int i = 0; i < teams.size(); i++) {
            //search team
            if (teams.get(i).getTeamName().equals( te2.getTeamName() )) {
                tempteam = teams.get(i);
            }
        }
        assertEquals(tempteam, te2);
    }

}
