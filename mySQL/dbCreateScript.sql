-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema dbTourGameManager
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema dbTourGameManager
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `dbTourGameManager` DEFAULT CHARACTER SET utf8 ;
USE `dbTourGameManager` ;

-- -----------------------------------------------------
-- Table `dbTourGameManager`.`Player`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dbTourGameManager`.`Player` ;

CREATE TABLE IF NOT EXISTS `dbTourGameManager`.`Player` (
  `idPlayer` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idPlayer`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbTourGameManager`.`Team`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dbTourGameManager`.`Team` ;

CREATE TABLE IF NOT EXISTS `dbTourGameManager`.`Team` (
  `idTeam` INT NOT NULL AUTO_INCREMENT,
  `teamName` VARCHAR(45) NOT NULL,
  `nElements` INT NULL,
  `imgsrc` VARCHAR(100) NULL,
  PRIMARY KEY (`idTeam`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbTourGameManager`.`Game`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dbTourGameManager`.`Game` ;

CREATE TABLE IF NOT EXISTS `dbTourGameManager`.`Game` (
  `idGame` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `minPlayer` INT NOT NULL,
  `maxPlayer` INT NOT NULL,
  `imgsrc` VARCHAR(100) NULL,
  PRIMARY KEY (`idGame`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbTourGameManager`.`Player_has_Team`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dbTourGameManager`.`Player_has_Team` ;

CREATE TABLE IF NOT EXISTS `dbTourGameManager`.`Player_has_Team` (
  `Player_idPlayer` INT NOT NULL,
  `Team_idTeam` INT NOT NULL,
  `nickname` VARCHAR(45) NULL,
  `lider` BLOB NULL,
  PRIMARY KEY (`Player_idPlayer`, `Team_idTeam`),
  INDEX `fk_Register_has_Team_Team1_idx` (`Team_idTeam` ASC),
  INDEX `fk_Register_has_Team_Register_idx` (`Player_idPlayer` ASC),
  CONSTRAINT `fk_Register_has_Team_Register`
    FOREIGN KEY (`Player_idPlayer`)
    REFERENCES `dbTourGameManager`.`Player` (`idPlayer`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Register_has_Team_Team1`
    FOREIGN KEY (`Team_idTeam`)
    REFERENCES `dbTourGameManager`.`Team` (`idTeam`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbTourGameManager`.`TourType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dbTourGameManager`.`TourType` ;

CREATE TABLE IF NOT EXISTS `dbTourGameManager`.`TourType` (
  `idTourType` INT NOT NULL AUTO_INCREMENT,
  `rules` VARCHAR(200) NULL,
  `nTeams` INT NULL,
  `maxTeams` INT NOT NULL,
  PRIMARY KEY (`idTourType`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbTourGameManager`.`Tour`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dbTourGameManager`.`Tour` ;

CREATE TABLE IF NOT EXISTS `dbTourGameManager`.`Tour` (
  `idTour` INT NOT NULL AUTO_INCREMENT,
  `TourType_idTourType` INT NOT NULL,
  `Game_idGame` INT NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `description` VARCHAR(250) NULL,
  `dateTime` DATETIME NOT NULL,
  `buyin` FLOAT NULL,
  `prizepool` FLOAT NULL,
  PRIMARY KEY (`idTour`),
  INDEX `fk_Tour_TourType1_idx` (`TourType_idTourType` ASC),
  INDEX `fk_Tour_Game1_idx` (`Game_idGame` ASC),
  CONSTRAINT `fk_Tour_TourType1`
    FOREIGN KEY (`TourType_idTourType`)
    REFERENCES `dbTourGameManager`.`TourType` (`idTourType`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Tour_Game1`
    FOREIGN KEY (`Game_idGame`)
    REFERENCES `dbTourGameManager`.`Game` (`idGame`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbTourGameManager`.`Tour_has_Team`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dbTourGameManager`.`Tour_has_Team` ;

CREATE TABLE IF NOT EXISTS `dbTourGameManager`.`Tour_has_Team` (
  `Tour_idTour` INT NOT NULL,
  `Team_idTeam` INT NOT NULL,
  `confirmation` TINYINT(1) NULL,
  PRIMARY KEY (`Tour_idTour`, `Team_idTeam`),
  INDEX `fk_Tour_has_Team_Team1_idx` (`Team_idTeam` ASC),
  INDEX `fk_Tour_has_Team_Tour1_idx` (`Tour_idTour` ASC),
  CONSTRAINT `fk_Tour_has_Team_Tour1`
    FOREIGN KEY (`Tour_idTour`)
    REFERENCES `dbTourGameManager`.`Tour` (`idTour`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Tour_has_Team_Team1`
    FOREIGN KEY (`Team_idTeam`)
    REFERENCES `dbTourGameManager`.`Team` (`idTeam`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbTourGameManager`.`TourHistory`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dbTourGameManager`.`TourHistory` ;

CREATE TABLE IF NOT EXISTS `dbTourGameManager`.`TourHistory` (
  `idTourHistory` INT NOT NULL AUTO_INCREMENT,
  `Tour_idTour` INT NOT NULL,
  `team` INT NULL,
  `pos` INT NULL,
  `prize` FLOAT NULL,
  PRIMARY KEY (`idTourHistory`),
  INDEX `fk_TourHistory_Tour1_idx` (`Tour_idTour` ASC),
  CONSTRAINT `fk_TourHistory_Tour1`
    FOREIGN KEY (`Tour_idTour`)
    REFERENCES `dbTourGameManager`.`Tour` (`idTour`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbTourGameManager`.`Match`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dbTourGameManager`.`Match` ;

CREATE TABLE IF NOT EXISTS `dbTourGameManager`.`Match` (
  `idMatch` INT NOT NULL AUTO_INCREMENT,
  `teamA` INT NOT NULL,
  `teamB` INT NOT NULL,
  `winner` INT NULL,
  `dateTime` DATETIME NOT NULL,
  PRIMARY KEY (`idMatch`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbTourGameManager`.`Tour_has_Match`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dbTourGameManager`.`Tour_has_Match` ;

CREATE TABLE IF NOT EXISTS `dbTourGameManager`.`Tour_has_Match` (
  `Tour_idTour` INT NOT NULL,
  `Match_idMatch` INT NOT NULL,
  PRIMARY KEY (`Tour_idTour`, `Match_idMatch`),
  INDEX `fk_Tour_has_Match_Match1_idx` (`Match_idMatch` ASC),
  INDEX `fk_Tour_has_Match_Tour1_idx` (`Tour_idTour` ASC),
  CONSTRAINT `fk_Tour_has_Match_Tour1`
    FOREIGN KEY (`Tour_idTour`)
    REFERENCES `dbTourGameManager`.`Tour` (`idTour`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Tour_has_Match_Match1`
    FOREIGN KEY (`Match_idMatch`)
    REFERENCES `dbTourGameManager`.`Match` (`idMatch`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbTourGameManager`.`Player_has_Tour`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dbTourGameManager`.`Player_has_Tour` ;

CREATE TABLE IF NOT EXISTS `dbTourGameManager`.`Player_has_Tour` (
  `Player_idPlayer` INT NOT NULL,
  `Tour_idTour` INT NOT NULL,
  PRIMARY KEY (`Player_idPlayer`, `Tour_idTour`),
  INDEX `fk_Player_has_Tour_Tour1_idx` (`Tour_idTour` ASC),
  INDEX `fk_Player_has_Tour_Player1_idx` (`Player_idPlayer` ASC),
  CONSTRAINT `fk_Player_has_Tour_Player1`
    FOREIGN KEY (`Player_idPlayer`)
    REFERENCES `dbTourGameManager`.`Player` (`idPlayer`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Player_has_Tour_Tour1`
    FOREIGN KEY (`Tour_idTour`)
    REFERENCES `dbTourGameManager`.`Tour` (`idTour`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
