-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema dbTourGameManagerTest
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema dbTourGameManagerTest
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `dbTourGameManagerTest` DEFAULT CHARACTER SET utf8 ;
USE `dbTourGameManagerTest` ;

-- -----------------------------------------------------
-- Table `dbTourGameManagerTest`.`Player`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dbTourGameManagerTest`.`Player` ;

CREATE TABLE IF NOT EXISTS `dbTourGameManagerTest`.`Player` (
  `idPlayer` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idPlayer`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbTourGameManagerTest`.`Team`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dbTourGameManagerTest`.`Team` ;

CREATE TABLE IF NOT EXISTS `dbTourGameManagerTest`.`Team` (
  `idTeam` INT NOT NULL AUTO_INCREMENT,
  `teamName` VARCHAR(45) NOT NULL,
  `nElements` INT NULL,
  `imgsrc` VARCHAR(100) NULL,
  PRIMARY KEY (`idTeam`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbTourGameManagerTest`.`Game`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dbTourGameManagerTest`.`Game` ;

CREATE TABLE IF NOT EXISTS `dbTourGameManagerTest`.`Game` (
  `idGame` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `minPlayer` INT NOT NULL,
  `maxPlayer` INT NOT NULL,
  `imgsrc` VARCHAR(100) NULL,
  PRIMARY KEY (`idGame`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbTourGameManagerTest`.`Player_has_Team`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dbTourGameManagerTest`.`Player_has_Team` ;

CREATE TABLE IF NOT EXISTS `dbTourGameManagerTest`.`Player_has_Team` (
  `Player_idPlayer` INT NOT NULL,
  `Team_idTeam` INT NOT NULL,
  `nickname` VARCHAR(45) NULL,
  `lider` BLOB NULL,
  PRIMARY KEY (`Player_idPlayer`, `Team_idTeam`),
  INDEX `fk_Register_has_Team_Team1_idx` (`Team_idTeam` ASC),
  INDEX `fk_Register_has_Team_Register_idx` (`Player_idPlayer` ASC),
  CONSTRAINT `fk_Register_has_Team_Register`
    FOREIGN KEY (`Player_idPlayer`)
    REFERENCES `dbTourGameManagerTest`.`Player` (`idPlayer`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Register_has_Team_Team1`
    FOREIGN KEY (`Team_idTeam`)
    REFERENCES `dbTourGameManagerTest`.`Team` (`idTeam`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbTourGameManagerTest`.`TourType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dbTourGameManagerTest`.`TourType` ;

CREATE TABLE IF NOT EXISTS `dbTourGameManagerTest`.`TourType` (
  `idTourType` INT NOT NULL AUTO_INCREMENT,
  `rules` VARCHAR(200) NULL,
  `nTeams` INT NULL,
  `maxTeams` INT NOT NULL,
  PRIMARY KEY (`idTourType`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbTourGameManagerTest`.`Tour`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dbTourGameManagerTest`.`Tour` ;

CREATE TABLE IF NOT EXISTS `dbTourGameManagerTest`.`Tour` (
  `idTour` INT NOT NULL AUTO_INCREMENT,
  `TourType_idTourType` INT NOT NULL,
  `Game_idGame` INT NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `description` VARCHAR(250) NULL,
  `dateTime` DATETIME NOT NULL,
  `buyin` FLOAT NULL,
  `prizepool` FLOAT NULL,
  PRIMARY KEY (`idTour`),
  INDEX `fk_Tour_TourType1_idx` (`TourType_idTourType` ASC),
  INDEX `fk_Tour_Game1_idx` (`Game_idGame` ASC),
  CONSTRAINT `fk_Tour_TourType1`
    FOREIGN KEY (`TourType_idTourType`)
    REFERENCES `dbTourGameManagerTest`.`TourType` (`idTourType`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Tour_Game1`
    FOREIGN KEY (`Game_idGame`)
    REFERENCES `dbTourGameManagerTest`.`Game` (`idGame`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbTourGameManagerTest`.`Tour_has_Team`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dbTourGameManagerTest`.`Tour_has_Team` ;

CREATE TABLE IF NOT EXISTS `dbTourGameManagerTest`.`Tour_has_Team` (
  `Tour_idTour` INT NOT NULL,
  `Team_idTeam` INT NOT NULL,
  `confirmation` TINYINT(1) NULL,
  PRIMARY KEY (`Tour_idTour`, `Team_idTeam`),
  INDEX `fk_Tour_has_Team_Team1_idx` (`Team_idTeam` ASC),
  INDEX `fk_Tour_has_Team_Tour1_idx` (`Tour_idTour` ASC),
  CONSTRAINT `fk_Tour_has_Team_Tour1`
    FOREIGN KEY (`Tour_idTour`)
    REFERENCES `dbTourGameManagerTest`.`Tour` (`idTour`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Tour_has_Team_Team1`
    FOREIGN KEY (`Team_idTeam`)
    REFERENCES `dbTourGameManagerTest`.`Team` (`idTeam`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbTourGameManagerTest`.`TourHistory`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dbTourGameManagerTest`.`TourHistory` ;

CREATE TABLE IF NOT EXISTS `dbTourGameManagerTest`.`TourHistory` (
  `idTourHistory` INT NOT NULL AUTO_INCREMENT,
  `Tour_idTour` INT NOT NULL,
  `team` INT NULL,
  `pos` INT NULL,
  `prize` FLOAT NULL,
  PRIMARY KEY (`idTourHistory`),
  INDEX `fk_TourHistory_Tour1_idx` (`Tour_idTour` ASC),
  CONSTRAINT `fk_TourHistory_Tour1`
    FOREIGN KEY (`Tour_idTour`)
    REFERENCES `dbTourGameManagerTest`.`Tour` (`idTour`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbTourGameManagerTest`.`Match`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dbTourGameManagerTest`.`Match` ;

CREATE TABLE IF NOT EXISTS `dbTourGameManagerTest`.`Match` (
  `idMatch` INT NOT NULL AUTO_INCREMENT,
  `teamA` INT NOT NULL,
  `teamB` INT NOT NULL,
  `winner` INT NULL,
  `dateTime` DATETIME NOT NULL,
  PRIMARY KEY (`idMatch`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbTourGameManagerTest`.`Tour_has_Match`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dbTourGameManagerTest`.`Tour_has_Match` ;

CREATE TABLE IF NOT EXISTS `dbTourGameManagerTest`.`Tour_has_Match` (
  `Tour_idTour` INT NOT NULL,
  `Match_idMatch` INT NOT NULL,
  PRIMARY KEY (`Tour_idTour`, `Match_idMatch`),
  INDEX `fk_Tour_has_Match_Match1_idx` (`Match_idMatch` ASC),
  INDEX `fk_Tour_has_Match_Tour1_idx` (`Tour_idTour` ASC),
  CONSTRAINT `fk_Tour_has_Match_Tour1`
    FOREIGN KEY (`Tour_idTour`)
    REFERENCES `dbTourGameManagerTest`.`Tour` (`idTour`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Tour_has_Match_Match1`
    FOREIGN KEY (`Match_idMatch`)
    REFERENCES `dbTourGameManagerTest`.`Match` (`idMatch`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbTourGameManagerTest`.`Player_has_Tour`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dbTourGameManagerTest`.`Player_has_Tour` ;

CREATE TABLE IF NOT EXISTS `dbTourGameManagerTest`.`Player_has_Tour` (
  `Player_idPlayer` INT NOT NULL,
  `Tour_idTour` INT NOT NULL,
  PRIMARY KEY (`Player_idPlayer`, `Tour_idTour`),
  INDEX `fk_Player_has_Tour_Tour1_idx` (`Tour_idTour` ASC),
  INDEX `fk_Player_has_Tour_Player1_idx` (`Player_idPlayer` ASC),
  CONSTRAINT `fk_Player_has_Tour_Player1`
    FOREIGN KEY (`Player_idPlayer`)
    REFERENCES `dbTourGameManagerTest`.`Player` (`idPlayer`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Player_has_Tour_Tour1`
    FOREIGN KEY (`Tour_idTour`)
    REFERENCES `dbTourGameManagerTest`.`Tour` (`idTour`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
