/* Player */
INSERT INTO `Player` (`username`, `password`, `email`) 
    VALUE ('user1', 'user1', 'user1@ua.pt');
INSERT INTO `Player` (`username`, `password`, `email`) 
    VALUE ('user2', 'user2', 'user2@ua.pt');
INSERT INTO `Player` (`username`, `password`, `email`) 
    VALUE ('user3', 'user3', 'user3@ua.pt');
INSERT INTO `Player` (`username`, `password`, `email`) 
    VALUE ('user4', 'user4', 'user4@ua.pt');
INSERT INTO `Player` (`username`, `password`, `email`) 
    VALUE ('user5', 'user5', 'user5@ua.pt');
select * from dbTourGameManager.Player;

/* Team */
INSERT INTO `Team` (`teamName`, `nElements`, `imgsrc`) 
	VALUE ('Team 1', 5, 'path/to/imgTeam1');
INSERT INTO `Team` (`teamName`, `nElements`, `imgsrc`) 
	VALUE ('Team 2', 3, 'path/to/imgTeam2');
INSERT INTO `Team` (`teamName`, `nElements`, `imgsrc`) 
	VALUE ('Team 3', 2, 'path/to/imgTeam3');
INSERT INTO `Team` (`teamName`, `nElements`, `imgsrc`) 
	VALUE ('Team 4', 5, 'path/to/imgTeam4');
select * from dbTourGameManager.Team;

/* Player_has_Team */
INSERT INTO `Player_has_Team` (`Player_idPlayer`, `Team_idTeam`, `nickname`, `lider`) 
	VALUE (1 ,1, 'Avatar 1', true);
INSERT INTO `Player_has_Team` (`Player_idPlayer`, `Team_idTeam`, `nickname`, `lider`) 
	VALUE (2 ,1, 'Avatar 2', false);
INSERT INTO `Player_has_Team` (`Player_idPlayer`, `Team_idTeam`, `nickname`, `lider`) 
	VALUE (2 ,2, 'Avatar 2', true);
INSERT INTO `Player_has_Team` (`Player_idPlayer`, `Team_idTeam`, `nickname`, `lider`) 
	VALUE (3 ,1, 'Avatar 3', false);
INSERT INTO `Player_has_Team` (`Player_idPlayer`, `Team_idTeam`, `nickname`, `lider`) 
	VALUE (3 ,3, 'Avatar 3', true);
INSERT INTO `Player_has_Team` (`Player_idPlayer`, `Team_idTeam`, `nickname`, `lider`) 
	VALUE (3 ,2, 'Avatar 3', false);
INSERT INTO `Player_has_Team` (`Player_idPlayer`, `Team_idTeam`, `nickname`, `lider`) 
	VALUE (4 ,2, 'Avatar 4', false);
INSERT INTO `Player_has_Team` (`Player_idPlayer`, `Team_idTeam`, `nickname`, `lider`) 
	VALUE (4 ,4, 'Avatar 4', true);
INSERT INTO `Player_has_Team` (`Player_idPlayer`, `Team_idTeam`, `nickname`, `lider`) 
	VALUE (5 ,4, 'Avatar 5', false);
select * from dbTourGameManager.Player_has_Team;

/* Game */
INSERT INTO `Game` (`name`, `minPlayer`, `maxPlayer`, `imgsrc`) 
	VALUE ('CS' , 5, 10, 'path/to/CSimg');
INSERT INTO `Game` (`name`, `minPlayer`, `maxPlayer`, `imgsrc`) 
	VALUE ('LOL' , 3, 5, 'path/to/LOLimg');
INSERT INTO `Game` (`name`, `minPlayer`, `maxPlayer`, `imgsrc`) 
	VALUE ('Call of Dutty' , 1, 10, 'path/to/CoDimg');
INSERT INTO `Game` (`name`, `minPlayer`, `maxPlayer`, `imgsrc`) 
	VALUE ('HearthStone' , 1, 1, 'path/to/HSimg');
select * from dbTourGameManager.Game;

/* TourType */
INSERT INTO `TourType` (`rules`, `nTeams`, `maxTeams`) 
	VALUE ('rules for TourType 1', 2, 12);
INSERT INTO `TourType` (`rules`, `nTeams`, `maxTeams`) 
	VALUE ('rules for TourType 2', 1, 6);
INSERT INTO `TourType` (`rules`, `nTeams`, `maxTeams`) 
	VALUE ('rules for TourType 3', 0, 3);
select * from dbTourGameManager.TourType;

/* Tour */
INSERT INTO `Tour` (`TourType_idTourType`, `Game_idGame`, `name`, `description`, `dateTime`, `buyin`, `prizepool`) 
	VALUE (1, 1, 'Torneio de CS', 'torneio do tipo 1 do jogo 1', NOW(), 5.0, 0.0);
INSERT INTO `Tour` (`TourType_idTourType`, `Game_idGame`, `name`, `description`, `dateTime`, `buyin`, `prizepool`) 
	VALUE (3, 2, 'Torneio de LoL', 'torneio do tipo 3 do jogo 2', NOW(), 10.0, 0.0);
INSERT INTO `Tour` (`TourType_idTourType`, `Game_idGame`, `name`, `description`, `dateTime`, `buyin`, `prizepool`) 
	VALUE (2, 4, 'Torneio de HearthStone', 'torneio do tipo 2 do jogo 4', NOW(), 2.5, 0.0);
INSERT INTO `Tour` (`TourType_idTourType`, `Game_idGame`, `name`, `description`, `dateTime`, `buyin`, `prizepool`) 
	VALUE (2, 4, 'Torneio de HearthStone', 'torneio do tipo 2 do jogo 4', NOW(), 3.5, 0.0);
select * from dbTourGameManager.Tour;

/* Tour_has_Team */
INSERT INTO `Tour_has_Team` (`Tour_idTour`, `Team_idTeam`, `confirmation`) 
	VALUE (1 , 1, false);
INSERT INTO `Tour_has_Team` (`Tour_idTour`, `Team_idTeam`, `confirmation`) 
	VALUE (1 , 2, true);
INSERT INTO `Tour_has_Team` (`Tour_idTour`, `Team_idTeam`, `confirmation`) 
	VALUE (3 , 3, true);
INSERT INTO `Tour_has_Team` (`Tour_idTour`, `Team_idTeam`, `confirmation`) 
	VALUE (4 , 4, true);
select * from dbTourGameManager.Tour_has_Team;

/* TourHistory */
INSERT INTO `TourHistory` (`Tour_idTour`, `team`, `pos`, `prize`) 
	VALUE (1 , 1, 1, 10.0);
INSERT INTO `TourHistory` (`Tour_idTour`, `team`, `pos`, `prize`) 
	VALUE (1 , 2, 2, 0.0);
select * from dbTourGameManager.TourHistory;

/* Match */
INSERT INTO `Match` (`teamA`, `teamB`, `winner`, `dateTime`) 
	VALUE (1 , 2, 1, NOW());
select * from dbTourGameManager.Match;
    
/* Tour_has_Match */
INSERT INTO `Tour_has_Match` (`Tour_idTour`, `Match_idMatch`) 
	VALUE (1 , 1);
select * from dbTourGameManager.Tour_has_Match;
    
/* Player_has_Tour */
INSERT INTO `Player_has_Tour` (`Player_idPlayer`, `Tour_idTour`) 
	VALUE (2 , 1);
INSERT INTO `Player_has_Tour` (`Player_idPlayer`, `Tour_idTour`) 
	VALUE (5 , 2);
INSERT INTO `Player_has_Tour` (`Player_idPlayer`, `Tour_idTour`) 
	VALUE (3 , 3);
INSERT INTO `Player_has_Tour` (`Player_idPlayer`, `Tour_idTour`) 
	VALUE (5 , 4);
select * from dbTourGameManager.Player_has_Tour;
